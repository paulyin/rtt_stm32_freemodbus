/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-10-22     zhangsz      init first
 */

#ifndef __KEY_H__
#define __KEY_H__

#include <rtthread.h>
#include <drivers/pm.h>
#include <rthw.h>

void key_gpio_init(void);

#endif
