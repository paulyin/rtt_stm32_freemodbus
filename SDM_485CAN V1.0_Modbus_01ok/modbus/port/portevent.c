/******************** (C) COPYRIGHT 2013 **************************
 * 文件名  ：portevent.c
 * 描述    ：STM32 Modbus 移植测试程序         
 * 实验平台：STM32F103开发板
 * 库版本  ：ST3.5.0
 *
 * 作者    ：Zhangsz
 * 编写日期：2013-11-09
**********************************************************************************/
#include "mb.h"
#include "mbport.h"

/* --------------- Variables -----------*/
static eMBEventType eQueuedEvent;
static BOOL     xEventInQueue;

/* ------------ Start implementation ---*/
BOOL xMBPortEventInit( void )
{
    xEventInQueue = FALSE;
    return TRUE;
}

BOOL xMBPortEventPost( eMBEventType eEvent )
{
    xEventInQueue = TRUE;
    eQueuedEvent = eEvent;
    return TRUE;
}

BOOL xMBPortEventGet( eMBEventType * eEvent )
{
    BOOL xEventHappened = FALSE;

    if(xEventInQueue)
    {
        *eEvent = eQueuedEvent;
        xEventInQueue = FALSE;
        xEventHappened = TRUE;
    }
    return xEventHappened;
}
