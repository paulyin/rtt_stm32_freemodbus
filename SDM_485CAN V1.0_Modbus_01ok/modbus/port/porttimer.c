/******************** (C) COPYRIGHT 2013 **************************
 * 文件名  ：porttimer.c
 * 描述    ：STM32 Modbus 移植测试程序         
 * 实验平台：STM32F103开发板
 * 库版本  ：ST3.5.0
 *
 * 作者    ：Zhangsz
 * 编写日期：2013-11-09
**********************************************************************************/
#include "port.h"
#include "mb.h"
#include "mbport.h"

/* Timer ticks are counted in multiples of 50us. Therefore 20000 ticks are
 * one second.
 */
//#define MB_TIMER_TICKS          ( 20000L )

BOOL xMBPortTimersInit(USHORT usTim1Timeout50us)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    /* TIM2 clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Period = usTim1Timeout50us;   //下一个更新事件装入活动的自动重装载寄存器周期的值。它的取值必须在0x0000和0xFFFF之间。
    TIM_TimeBaseStructure.TIM_Prescaler = (uint16_t) (SystemCoreClock / 20000) - 1;  //预分频值
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //时钟分割
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //TIM向上计数模式
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TIM_ClearFlag(TIM2, TIM_FLAG_Update); //清除溢出中断标志
    TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
    //定时器2禁能
    TIM_Cmd(TIM2, DISABLE);

  return 1;
}

void vMBPortTimersEnable( void )
{
  TIM_ClearFlag(TIM2, TIM_FLAG_Update); //清除溢出中断标志
  TIM_SetCounter(TIM2,0x00);            //清零计数器值
  TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
  TIM_Cmd(TIM2,ENABLE);
}

void vMBPortTimersDisable( void )
{
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
    TIM_ITConfig(TIM2,TIM_IT_Update,DISABLE);
    TIM_SetCounter(TIM2, 0);
    TIM_Cmd(TIM2,DISABLE);
}

void prvvTIMERExpiredISR( void )
{
    (void)pxMBPortCBTimerExpired();
}

void TIM2_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
    {
        TIM_ClearFlag(TIM2, TIM_FLAG_Update);           //清中断标记
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);     //清除定时器T2溢出中断标志位
        prvvTIMERExpiredISR();
    }
}
