/******************** (C) COPYRIGHT 2013 **************************
 * 文件名  ：portserial.c
 * 描述    ：STM32 Modbus 从机程序
 * 实验平台：SDM_485CAN V1.0开发板
 * 库版本  ：ST3.5.0
 *
 * 作者    ：Zhangsz
 * 编写日期：2013-12-06
**********************************************************************************/
#include "port.h"

#include "mb.h"
#include "mbport.h"

void vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable)
{
    //ENTER_CRITICAL_SECTION(  );
    if( xRxEnable )
    {
        USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
        RS485_RECEIVE_MODE;
    }
    else
    {
       USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
       RS485_SEND_MODE;
    }
    if( xTxEnable )
    {
       USART_ITConfig(USART3, USART_IT_TC, ENABLE);
    }
    else
    {
       USART_ITConfig(USART3, USART_IT_TC, DISABLE);
    }
   // EXIT_CRITICAL_SECTION(  );
}

void vMBPortClose(void)
{
    USART_ITConfig(USART3, USART_IT_TXE | USART_IT_RXNE, DISABLE);
    USART_Cmd(USART3, DISABLE);
}


BOOL xMBPortSerialInit(UCHAR ucPort, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity)
{
    BOOL bInitialized = TRUE;
    GPIO_InitTypeDef GPIO_InitStruct; 
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB
        | RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3 ,ENABLE);

    /*USART1  */
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB,&GPIO_InitStruct);

    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_11;
    //GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB,&GPIO_InitStruct);

    //GPIOA.8
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8; 
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; 
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOA, &GPIO_InitStruct); 

    USART_InitStructure.USART_BaudRate = ulBaudRate;

    switch ( eParity )
    {
    case MB_PAR_NONE:
        USART_InitStructure.USART_Parity = USART_Parity_No;
        break;
    case MB_PAR_ODD:
        USART_InitStructure.USART_Parity = USART_Parity_Odd;
        break;
    case MB_PAR_EVEN:
        USART_InitStructure.USART_Parity = USART_Parity_Even;
        break;
    }

    switch ( ucDataBits )
    {
    case 8:
        if(eParity==MB_PAR_NONE)
        USART_InitStructure.USART_WordLength = USART_WordLength_8b;
        else
        USART_InitStructure.USART_WordLength = USART_WordLength_9b;
        break;
    case 7:
        break;
    default:
        bInitialized = FALSE;
    }

    if( bInitialized )
    {

        USART_InitStructure.USART_StopBits = USART_StopBits_1;
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
        USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

        ENTER_CRITICAL_SECTION(  );
        USART_Init(USART3, &USART_InitStructure);
        USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
        USART_Cmd(USART3, ENABLE);
        USART_ClearFlag(USART3, USART_FLAG_TC);     

        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
        NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

        NVIC_Init(&NVIC_InitStructure);

        EXIT_CRITICAL_SECTION(  );
    }
    return bInitialized;
}

BOOL xMBPortSerialPutByte( CHAR ucByte )
{
    USART_SendData(USART3, ucByte);
    while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == 0);  //等待发送完成
    return TRUE;
}

BOOL xMBPortSerialGetByte( CHAR * pucByte )
{
    //USART_ClearFlag(USART3, USART_IT_RXNE);  //中断接收
    //while(USART_GetFlagStatus(USART1, USART_IT_RXNE) == 0); //等待接收标志
    *pucByte = (u8)USART_ReceiveData(USART3); //获取接收BUFFER的数据
    return TRUE;
}

void UART3_IRQ(void)
{
    if(USART_GetITStatus(USART3,USART_IT_RXNE))
    {
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
        pxMBFrameCBByteReceived(  );
    }
    else if(USART_GetITStatus(USART3,USART_IT_TC))
    {
        USART_ClearITPendingBit(USART3, USART_IT_TC);
        pxMBFrameCBTransmitterEmpty(  );
    }
}

void EnterCriticalSection( void )
{
    __disable_irq();
}

void ExitCriticalSection( void )
{
    __enable_irq();
}
