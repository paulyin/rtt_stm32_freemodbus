/******************** (C) COPYRIGHT 2013 **************************
 * 文件名  ：main.c
 * 描述    ：STM32 Modbus 从机程序         
 * 实验平台：SDM_485CAN V1.0开发板
 * 库版本  ：ST3.5.0
 *
 * 作者    ：Zhangsz
 * 编写日期：2013-12-06
**********************************************************************************/
#include <stdio.h>
#include "stm32f10x.h"
#include "mb.h"

void GPIO_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_12 | GPIO_Pin_13 |GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB,&GPIO_InitStructure);

    GPIO_SetBits(GPIOB,GPIO_Pin_8); //打开继电器K2
    GPIO_SetBits(GPIOB,GPIO_Pin_9); //打开继电器K1
    GPIO_SetBits(GPIOB,GPIO_Pin_13); //LG3 灭
    GPIO_SetBits(GPIOB,GPIO_Pin_14); //LG4 灭
    GPIO_ResetBits(GPIOB,GPIO_Pin_12); //LG2 亮
    GPIO_ResetBits(GPIOB,GPIO_Pin_15); //LG5 亮
}

/* 
 * 函数名：main
 * 描述  : "主机"的主函数
 * 输入  ：无
 * 输出  : 无
 */
int main(void)
{
    //NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
    //NVIC_Configuration();
    GPIO_Config();
    eMBInit( MB_RTU, 0x0A, 2, 115200, MB_PAR_NONE );

    /* Enable the Modbus Protocol Stack. */
    eMBEnable();

    while(1)
    {
        (void)eMBPoll();
    }
}




/******************* (C) (C) COPYRIGHT 2013  *****END OF FILE************/
