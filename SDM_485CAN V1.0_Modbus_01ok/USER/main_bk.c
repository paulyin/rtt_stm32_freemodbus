/******************** (C) COPYRIGHT 2013 **************************
 * 文件名  ：main.c
 * 描述    ：STM32 Modbus 移植测试程序         
 * 实验平台：火牛STM32开发板
 * 库版本  ：ST3.5.0
 *
 * 作者    ：Zhangsz
 * 编写日期：2013-11-11
**********************************************************************************/
#include <stdio.h>
#include "stm32f10x.h"
#include "mb.h"

uint8_t seg_comA[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7F,0x6F,0x77,0x7C,0x39,0x5E,0x79,0x71}; // 显示段码值0123456789ABCDEF	 共阴
uint8_t seg_comC[]={0xc0,0xF9,0xa4,0xb0,0x99,0x92,0x82,0xf8,0x80,0x90,0x88,0x83,0xc6,0xa1,0x86,0x8e}; // 显示段码值0123456789ABCDEF  共阳


uint8_t 		slave_addr;	 //从机地址：拨码设置
uint32_t 		slave_baud;	 //从机波特率
uint8_t 		select_baud;  //从机波特率：拨码设置
uint8_t			seg_mode;	 //数码管的共阴或是共阳模式。

/* ----------------------- Holding register Defines ------------------------------------------*/

#define REG_HOLDING_START 1000
#define REG_HOLDING_NREGS 4

/* ----------------------- Static variables ---------------------------------*/
static unsigned short usRegHoldingStart = REG_HOLDING_START;
static unsigned short usRegHoldingBuf[REG_HOLDING_NREGS];


/* ----------------------- coils register Defines ------------------------------------------*/
#define REG_COILS_START     1000
#define REG_COILS_SIZE      16

/* ----------------------- Static variables ---------------------------------*/
static unsigned char ucRegCoilsBuf[REG_COILS_SIZE / 8];

/* ----------------------- discrete register Defines ------------------------------------------*/
#define REG_DISC_START     1000
#define REG_DISC_SIZE      16

/* ----------------------- Static variables ---------------------------------*/
static unsigned char ucRegDiscBuf[REG_DISC_SIZE / 8] = { 0x98, 0x6e };

/* ----------------------- input register Defines ------------------------------------------*/
#define REG_INPUT_START 1000
#define REG_INPUT_NREGS 2

/* ----------------------- Static variables ---------------------------------*/
static unsigned short usRegInputStart = REG_INPUT_START;
static unsigned short usRegInputBuf[REG_INPUT_NREGS]={0x4525,0x1324};

eMBErrorCode
eMBRegInputCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    int             iRegIndex;

    if( ( usAddress >= REG_INPUT_START )
        && ( usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS ) )
    {
        iRegIndex = ( int )( usAddress - usRegInputStart );
        while( usNRegs > 0 )
        {
            *pucRegBuffer++ =
                ( unsigned char )( usRegInputBuf[iRegIndex] >> 8 );
            *pucRegBuffer++ =
                ( unsigned char )( usRegInputBuf[iRegIndex] & 0xFF );
            iRegIndex++;
            usNRegs--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}
eMBErrorCode
eMBRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils,
               eMBRegisterMode eMode )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    int             iNCoils = ( int )usNCoils;
    unsigned short  usBitOffset;

    /* Check if we have registers mapped at this block. */
    if( ( usAddress >= REG_COILS_START ) &&
        ( usAddress + usNCoils <= REG_COILS_START + REG_COILS_SIZE ) )
    {
        usBitOffset = ( unsigned short )( usAddress - REG_COILS_START );
        switch ( eMode )
        {
                /* Read current values and pass to protocol stack. */
            case MB_REG_READ:
                while( iNCoils > 0 )
                {
                    *pucRegBuffer++ =
                        xMBUtilGetBits( ucRegCoilsBuf, usBitOffset,
                                        ( unsigned char )( iNCoils >
                                                           8 ? 8 :
                                                           iNCoils ) );
                    iNCoils -= 8;
                    usBitOffset += 8;
                }
                break;

                /* Update current register values. */
            case MB_REG_WRITE:
                while( iNCoils > 0 )
                {
                    xMBUtilSetBits( ucRegCoilsBuf, usBitOffset, 
                                    ( unsigned char )( iNCoils > 8 ? 8 : iNCoils ),
                                    *pucRegBuffer++ );
                    iNCoils -= 8;
                    usBitOffset += 8;
                }
                break;
        }

    }
    else
    {
        eStatus = MB_ENOREG;
    }
    return eStatus;
}

eMBErrorCode
eMBRegDiscreteCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    short           iNDiscrete = ( short )usNDiscrete;
    unsigned short  usBitOffset;

    /* Check if we have registers mapped at this block. */
    if( ( usAddress >= REG_DISC_START ) &&
        ( usAddress + usNDiscrete <= REG_DISC_START + REG_DISC_SIZE ) )
    {
        usBitOffset = ( unsigned short )( usAddress - REG_DISC_START );
        while( iNDiscrete > 0 )
        {
            *pucRegBuffer++ =
                xMBUtilGetBits( ucRegDiscBuf, usBitOffset,
                                ( unsigned char )( iNDiscrete >
                                                   8 ? 8 : iNDiscrete ) );
            iNDiscrete -= 8;
            usBitOffset += 8;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }
    return eStatus;
}

eMBErrorCode
eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs,
                 eMBRegisterMode eMode )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    int             iRegIndex;

    if( ( usAddress >= REG_HOLDING_START ) &&
        ( usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS ) )
    {
        iRegIndex = ( int )( usAddress - usRegHoldingStart );
        switch ( eMode )
        {
                /* Pass current register values to the protocol stack. */
            case MB_REG_READ:
                while( usNRegs > 0 )
                {
                    *pucRegBuffer++ =
                        ( unsigned char )( usRegHoldingBuf[iRegIndex] >> 8 );
                    *pucRegBuffer++ =
                        ( unsigned char )( usRegHoldingBuf[iRegIndex] &
                                           0xFF );
                    iRegIndex++;
                    usNRegs--;
                }
                break;

                /* Update current register values with new values from the
                 * protocol stack. */
            case MB_REG_WRITE:
                while( usNRegs > 0 )
                {
                    usRegHoldingBuf[iRegIndex] = *pucRegBuffer++ << 8;
                    usRegHoldingBuf[iRegIndex] |= *pucRegBuffer++;
                    iRegIndex++;
                    usNRegs--;
                }
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }
    return eStatus;
}

/**
  * @brief  Configure the nested vectored interrupt controller.
  * @param  None
  * @retval : None
  */
void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable the TIM2 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

  NVIC_Init(&NVIC_InitStructure);

  /* Enable the TIM2 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

  NVIC_Init(&NVIC_InitStructure);
}


void GPIO_Config(void)
{
 	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);


	/**数据线：PA0~PA7  485方向控制:PA8   推挽输出方式**/
	GPIO_InitStructure.GPIO_Pin  	= GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |  \
	 								  GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 ;
	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);


	/**从机地址拨码设置：PA12~PA15    浮空输入方式  **/
	GPIO_InitStructure.GPIO_Pin  	= GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_IN_FLOATING;
	//GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);


	/**锁存器片选线：PB3~PB6,PB12~PB15    推挽输出方式**/
	GPIO_InitStructure.GPIO_Pin  	= GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 |  \
	 								  GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_2MHz;
	GPIO_Init(GPIOB,&GPIO_InitStructure);


	/**模式选择：PB7,波特率拨码设置:PB8 PB9    浮空输入方式**/
	GPIO_InitStructure.GPIO_Pin  	= GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_IN_FLOATING;
	//GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
}


void DataBus_Out(GPIO_TypeDef* GPIOx,uint8_t Data)

{
    GPIOx->ODR &= 0XFFF0;
    GPIOx->ODR |= Data;
}


void seg_reset(uint8_t seg_mode)	//程序初始化前关闭数码管显示
{
	switch(seg_mode)
	{
		case 0x00:	//共阳 ON
			GPIO_ResetBits(GPIOA,GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | \
	 						GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7);
			GPIO_ResetBits(GPIOB,	GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | \
	 						GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
			GPIO_SetBits(GPIOB,	GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | \
	 						GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
			break;
		case 0x01:  //共阴 OFF
			GPIO_SetBits(GPIOA,GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | \
	 						GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7);
			GPIO_ResetBits(GPIOB,	GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | \
	 						GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
			GPIO_SetBits(GPIOB,	GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | \
	 						GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
			break;
	}
}


void seg_display(uint16_t *segbuffer,uint8_t dis_mode)
{
   	//uint8_t i;
	switch(dis_mode)
	{
	case 0x00: 
		GPIO_ResetBits(GPIOB,	GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | \
	 						GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);	   //低电平，锁存前面的
		
		
		if((segbuffer[2]>>12)&0x0f)
		DataBus_Out(GPIOA,(seg_comA[(segbuffer[0]>>12)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comA[(segbuffer[0]>>12)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_3);		   //高电平，数据锁存

		if((segbuffer[2]>>8)&0x0f)
		DataBus_Out(GPIOA,(seg_comA[(segbuffer[0]>>8)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comA[(segbuffer[0]>>8)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_4);		   //高电平，数据锁存

		if((segbuffer[2]>>4)&0x0f)
		DataBus_Out(GPIOA,(seg_comA[(segbuffer[0]>>4)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comA[(segbuffer[0]>>4)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_5);		   //高电平，数据锁存

		if((segbuffer[2])&0x0f)
		DataBus_Out(GPIOA,(seg_comA[(segbuffer[0])&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comA[(segbuffer[0])&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_6);		   //高电平，数据锁存

		if((segbuffer[3]>>12)&0x0f)
		DataBus_Out(GPIOA,(seg_comA[(segbuffer[1]>>12)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comA[(segbuffer[1]>>12)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_12);		   //高电平，数据锁存

		if((segbuffer[3]>>8)&0x0f)
		DataBus_Out(GPIOA,(seg_comA[(segbuffer[1]>>8)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comA[(segbuffer[1]>>8)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_13);		   //高电平，数据锁存

		if((segbuffer[3]>>4)&0x0f)
		DataBus_Out(GPIOA,(seg_comA[(segbuffer[1]>>4)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comA[(segbuffer[1]>>4)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_14);		   //高电平，数据锁存

		if((segbuffer[3])&0x0f)
		DataBus_Out(GPIOA,(seg_comA[(segbuffer[1])&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comA[(segbuffer[1])&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_15);		   //高电平，数据锁存
		
	break;

	case 0x01:
	 		GPIO_ResetBits(GPIOB,	GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | \
	 						GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);	   //低电平，锁存前面的
		
		
		if((segbuffer[2]>>12)&0x0f)
		DataBus_Out(GPIOA,(seg_comC[(segbuffer[0]>>12)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comC[(segbuffer[0]>>12)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_3);		   //高电平，数据锁存

		if((segbuffer[2]>>8)&0x0f)
		DataBus_Out(GPIOA,(seg_comC[(segbuffer[0]>>8)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comC[(segbuffer[0]>>8)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_4);		   //高电平，数据锁存

		if((segbuffer[2]>>4)&0x0f)
		DataBus_Out(GPIOA,(seg_comC[(segbuffer[0]>>4)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comC[(segbuffer[0]>>4)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_5);		   //高电平，数据锁存

		if((segbuffer[2])&0x0f)
		DataBus_Out(GPIOA,(seg_comC[(segbuffer[0])&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comC[(segbuffer[0])&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_6);		   //高电平，数据锁存

		if((segbuffer[3]>>12)&0x0f)
		DataBus_Out(GPIOA,(seg_comC[(segbuffer[1]>>12)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comC[(segbuffer[1]>>12)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_12);		   //高电平，数据锁存

		if((segbuffer[3]>>8)&0x0f)
		DataBus_Out(GPIOA,(seg_comC[(segbuffer[1]>>8)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comC[(segbuffer[1]>>8)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_13);		   //高电平，数据锁存

		if((segbuffer[3]>>4)&0x0f)
		DataBus_Out(GPIOA,(seg_comC[(segbuffer[1]>>4)&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comC[(segbuffer[1]>>4)&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_14);		   //高电平，数据锁存

		if((segbuffer[3])&0x0f)
		DataBus_Out(GPIOA,(seg_comC[(segbuffer[1])&0x0f])|0x80);
		else
	    DataBus_Out(GPIOA,seg_comC[(segbuffer[1])&0x0f]);
		GPIO_SetBits(GPIOB,	GPIO_Pin_15);		   //高电平，数据锁存

	break; 
	}
}




/* 
 * 函数名：main
 * 描述  : "主机"的主函数
 * 输入  ：无
 * 输出  : 无
 */
int main(void)
{
   
   	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_Configuration();
	GPIO_Config();
	slave_addr = (GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_12)<<3) | \
				 (GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_13)<<2) | \
				 (GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_14)<<1) | \
				 GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_15);
	//slave_addr+=1;
	/*从机地址是：0~F 15个有效，一个为广播呢？还是加1，为1~16号从站地址。*/ 
	select_baud = (GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_8)<<1) | GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_9);
	seg_mode 	= 	GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_7);

   	seg_reset(seg_mode);


	switch(select_baud&0x03)
	{
		case 0x00:slave_baud=9600;break; //2400   ON ON
		case 0x01:slave_baud=19200;break; //4800   ON OFF
		case 0x02:slave_baud=38400;break; //9600   OFF ON
		case 0x03:slave_baud=115200;break; //19200  0FF OFF
	}
				 		
 	//eMBInit( MB_RTU, slave_addr, 2, slave_baud, MB_PAR_NONE );
	eMBInit( MB_RTU, 0x01, 1, 38400, MB_PAR_NONE );
	/* Enable the Modbus Protocol Stack. */
	eMBEnable(  );
	while(1)
	{
	 ( void )eMBPoll(  );
	  seg_display(usRegHoldingBuf,seg_mode);   //数码管显示缓冲区数据，这里没有中断，不知道会不会出现问题。
	}
	
}




/******************* (C) COPYRIGHT 2012 WildFire Team *****END OF FILE************/
