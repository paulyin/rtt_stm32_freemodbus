# rtt_stm32_freemodbus

#### 介绍

基于rt-thread stm32f103c8t6 freemodbus RTU（RS485）功能的验证

* 串口3为RS485
* 串口1 为MSH串口

2020-12-01
* 移植freemodbus RS485 RTU 从机成功

2020-12-02
* 移植freemodbus RS485 RTU 从机成功

* 问题点：

```
msh >free
total memory: 14280
used memory : 7468
maximum allocated memory: 7468

```
STM32F103C8T6，手册上写着有20K，但这里只有14K左右。后面有时间看看为什么！！

