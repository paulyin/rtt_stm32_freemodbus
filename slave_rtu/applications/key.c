/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-10-22     zhangsz      init first
 */

#include "key.h"
#include "drv_gpio.h"
#include "board.h"

#define DBG_ENABLE
#define DBG_SECTION_NAME    "key"
#define DBG_LEVEL           DBG_LOG
#include <rtdbg.h>

#define PIN_KEY0    GET_PIN(B,5)


void key0_irq_callback(void *parameter)
{
    static uint8_t key0_status = 0x00;

    key0_status ^= 0x01;

    //if(key0_status == 0x00)
        //rt_pm_module_release(PM_BOARD_ID, PM_SLEEP_MODE_IDLE);
    //else
        //rt_pm_module_request(PM_BOARD_ID, PM_SLEEP_MODE_IDLE);

    LOG_D("[key0_irq]\n");
}


void key_gpio_init(void)
{
    LOG_D("key_gpio_init.\n");

    /* set key pin mode to input */
    LOG_D("PIN_KEY0=%d\n", PIN_KEY0);

    rt_pin_mode(PIN_KEY0, PIN_MODE_INPUT_PULLUP);

    /* set interrupt mode and attach interrupt callback function */
    rt_pin_attach_irq(PIN_KEY0, PIN_IRQ_MODE_FALLING, key0_irq_callback, NULL);

    /* enable interrupt */
    rt_pin_irq_enable(PIN_KEY0, PIN_IRQ_ENABLE);
}
